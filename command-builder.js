const path = require('path');
const bumpVersion = require('./bumpVersion');
const prepPackage = require('./prepPackage');
const deployApp = require('./deploy-app');
const zipper = require('./zipper');
const setPath = require('./set-path').setPath;
const processPath = process.cwd();

module.exports = { buildCommand }

const builders = { build, serve, test, promote, deploy, zip, setpath, jshint };

function buildCommand(options) {
    return builders[options.task](options);
}

function jshint(options) {
    let config = path.join(processPath, "/node_modules/webpack-setup/.jshintrc");
    let ignore = "./src/.jshintignore";
    let defaultPath = "src";
    let jshintSrc = ((options.jshint) == true) ? defaultPath : defaultPath + "/" + `${options.jshint}`;
    return `jshint ${jshintSrc} --config ${config} --exclude-path ${ignore}`;
}

function build(options) {
    let config = path.join(__dirname, 'webpack.build.js');
    let analyze = options.analyze ? '--env.analyze' : '';
    let mode = options.mode == 'prod' ? 'production' : 'development';
    let hook = options.hook ? ` --env.hook ${options.hook}` : '';

    return `webpack --mode ${mode} --config ${config} --progress --colors ${analyze} ${hook}`;
}

function serve(options) {
    let config = path.join(__dirname, 'webpack.serve.js');
    let open = options.open ? '--open' : '';
    let watch = options.watch ? '--watch' : '';
    let serveqa = options.serveqa ? '--serveqa' : '';
    let hook = options.hook ? ` --hook ${options.hook}` : '';

    return `webpack-dev-server --progress --colors --config "${config}" ${open} ${watch} ${serveqa} ${hook}`;
}

function test(options) {
    let config = path.join(__dirname, 'karma.config.js');
    let watch = options.watch ? ' --auto-watch --no-single-run' : '';
    let hook = options.hook ? ` --hook ${options.hook}` : '';

    return `karma start "${config}" ${watch} ${hook} --color`;
}

function promote(options) {
    const version = options.version || 'patch';
    const index = version === 'major' ? 0 : version === 'minor' ? 1 : 2;
    const stats = bumpVersion(index);
    const { dir, dest } = prepPackage();
    const zipFileName = `${getModuleName(stats.name)}-${stats.version}.tgz`;
    const zipFilePath = path.join(dir, zipFileName);
    return `npm pack "${dir}" && cpx ${zipFileName} "${dest}" && rimraf "${dir}" && del /F ${zipFileName}`;
}

function setpath(options) {
    return () => setPath(options.enterprisePath);
}

function getModuleName(moduleName) {
    if (!moduleName) {
        return moduleName;
    }
    return moduleName.replace("@", "").replace("/", "-");
}


function deploy(options) {
    return () => deployApp.deploy(options);
}

function zip(options) {
    return () => { zipper.zip(options) };
}