const fs = require('fs');

module.exports = { getAppName };

function getAppName(packageJson) {
    let f = JSON.parse(fs.readFileSync(packageJson));
    let name = f.name.split('.');
    if (name.length > 1) {
        let newNames = name.map(n => {
            return capitalize(n);
        });
        return newNames.join('.');
    }
    return name;
};

function capitalize(str) {
    return str.charAt(0).toUpperCase() + str.slice(1).toLowerCase();
}