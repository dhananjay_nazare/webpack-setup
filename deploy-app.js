const path = require('path');
const fs = require('fs');
const fsExtra = require('fs-extra');
const getAppName = require('./common').getAppName;
const processPath = process.cwd();

module.exports = { deploy };

function deploy(options) {
    options = options || {};
    var packageJson = path.join(processPath, 'package.json');
    let appName = options.appName || getAppName(packageJson);
    let src = options.appSrc || 'dist';
    const branch = options.branch || 'Trunk';
    let enterpriseDir = `c:/code/${branch}/Enterprise`;

    src = path.join(processPath, src);

    let baseDir = getDeployDirectory(enterpriseDir);

    let deployDir = path.join(baseDir, appName);

    fsExtra.emptyDir(deployDir, err => {
        if (err) {
            console.log(err);
            process.exit(-1);
        }
        console.log('deploying ' + src + ' to ' + deployDir);
        fsExtra.copy(src, deployDir, err => {
            if (err) {
                console.log(err);
                process.exit(-1);
            } else {
                console.log('app deployed');
                process.exit(0);
            }
        });
    });
}

function getDeployDirectory(enterpriseDir) {
    let file = path.join(processPath, 'projectConfig.json');
    let exists = fs.existsSync(file);
    let dir = enterpriseDir;
    if (exists) {
        let json = JSON.parse(fs.readFileSync(file));
        dir = json.enterpriseDirectory;

    }
    return path.join(dir, 'Products', 'Site', 'StormsPackages');
}