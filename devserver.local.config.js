
module.exports = {
    contentBase: './src/public',
    stats: 'minimal',
    proxy: {
        "/StarsOne/*": {
            "context": "/Trunk/",
            "host": "localhost",
            "target": "http://localhost",
            "port": 80,
            "secure": false,
            "changeOrigin": true,
            "pathRewrite": { "^/StarsOne": "/Trunk" },
            "headers": { "Stars-Authentication": "RMIS_MULTICLIENT:OWNER:OWNER:ABC" }
        }
    }
};
