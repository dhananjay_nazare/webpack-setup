const webpack = require('webpack');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const LessGlobPlugin = require('less-glob-loader/plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

const processPath = process.cwd();

const PLUGINS = {
  HtmlWebPackPlugin: 'HtmlWebPackPlugin',
  MiniCssExtractPlugin: 'MiniCssExtractPlugin',
  LessGlobPlugin: 'LessGlobPlugin',
  NoEmitOnErrorsPlugin: 'NoEmitOnErrorsPlugin',
  CleanWebpackPlugin: 'CleanWebpackPlugin',
  CopyWebpackPlugin: 'CopyWebpackPlugin',
  UglifyJsPlugin: 'UglifyJsPlugin'
};

module.exports = { getPlugin, PLUGINS };

function getPlugin(name, mode) {
  switch (name) {
    case PLUGINS.HtmlWebPackPlugin:
      return new HtmlWebPackPlugin({
        template: 'index.html',
        filename: 'index.html'
      });
    case PLUGINS.MiniCssExtractPlugin:
      return new MiniCssExtractPlugin({
        filename: '[name].[hash].css',
        chunkFilename: '[id].[hash].css'
      });
    case PLUGINS.LessGlobPlugin:
      return new LessGlobPlugin({
        exclude: /app.less|SpecRunner.less|test.less|cs-common.less/
      });
    case PLUGINS.NoEmitOnErrorsPlugin:
      return new webpack.NoEmitOnErrorsPlugin();
    case PLUGINS.CleanWebpackPlugin:
      return new CleanWebpackPlugin(['dist'], { root: processPath });
    case PLUGINS.CopyWebpackPlugin:
      return new CopyWebpackPlugin([{ from: './help', to: './help' }]);
    case PLUGINS.UglifyJsPlugin:
      return new UglifyJsPlugin({ uglifyOptions: { output: { comments: false } } });
  }
  throw `Webpack plugin '${name}' not found!!`;
}
