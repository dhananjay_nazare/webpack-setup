const rimraf = require("rimraf").sync;
const fs = require("fs");
const glob = require("glob");
const mkd = require("mkdirp");
let path = require("path");

const ignoreArray = [
  "**/*.spec.js",
  "package",
  "node_modules",
  "coverage",
  "coverage/**/*",
  "node_modules/**/*",
  "**/*.tgz",
  "**/Gruntfile.js",
  "**/templates.js",
  "test.webpack.js",
  "**/karma.conf.js",
  "**/SpecRunner.less",
  "**/SpecRunner.html"
];

function doesDirExist(dir) {
  try {
    fs.accessSync(dir, fs.constants.F_OK);
    return true;
  } catch (e) {
    return false;
  }
}

function makeDir(dir) {
  if (doesDirExist(dir)) {
    rimraf(dir);
  }

  fs.mkdirSync(dir);
}

function getTagsFolder() {
  try {
    const stats = fs.statSync("../../tags");
    if (stats.isDirectory()) {
      return "../../tags";
    }

    return "../tags";
  } catch (e) {
    return "../tags";
  }
}

module.exports = function prepPackage() {
  const dir = path.join(process.cwd(), "package");
  makeDir(dir);
  const files = [
    ...glob.sync("**/*.{js,html,less,png,jpg,jpeg,gif,svg,woff,woff2,ttf,eot,ico}", { ignore: ignoreArray }),
    "package.json",
    "tsconfig.json"
  ];

  console.log("...copying files");

  for (const i of files) {
    if (fs.lstatSync(i).isFile()) {
      const newDir = path.join(dir, i);
      const dirname = path.dirname(newDir);
      if (!doesDirExist(dirname)) {
        mkd.sync(dirname);
      }
      fs.copyFileSync(i, newDir);
    } else {
      mkd.sync(path.join(dir, i));
    }
  }

  return { dir: dir, dest: getTagsFolder() };
};
