#!/usr/bin/env node

let program = require('commander');
let path = require('path');
let fs = require('fs-extra');
var webpack_setup = require('./');
const validModes = ['prod', 'dev'];
const validTasks = ['build', 'serve', 'test', 'promote', 'deploy', 'zip', 'setpath', 'jshint'];
const validVersions = ['patch', 'minor', 'major'];

program
    .version('0.1.1')
    .description(`webpack-setup provides command line options for required webpack setup in production/development/testing mode with default configuration which godd enough for most of scenarios. It also provide interface to override default configuration.`)
    .option('-T, --task [type]', 'Run webpack task build/serve/test/promote/setpath/jshint')
    .option('-M, --mode [type]', 'Run webpack in prod/dev mode')
    .option('-O, --open [type]', 'Open browser in dev mode')
    .option('-Q, --qa', 'Run webpack dev server with QA/local setup')
    .option('-A, --analyze', 'Run analyzer to review package structure')
    .option('-H --hook [value]', 'Provide JS file name which will be hooked in the webpack execution process to allowed custom changes')
    .option('-W, --watch', 'Watch for changes')
    .option('-P, --promoteVersion [value]', 'Promote Changes with options major/minor/path')
    .option('--appName [value]', 'App name to be used for deployment')
    .option('--appSrc [value]', 'Source folder path to be deployed')
    .option('--branch [value]', 'Source branch to be considered for deployment')
    .option('--zipTarget [value]', 'Path where zipped deployment package to created')
    .option('--enterprisePath [value]', 'Path of enterprise folder')
    .option('-J, --jshint [value]', 'Source folder path to perform jshint')
    .parse(process.argv);

if (program.task && !validTasks.includes(program.task)) {
    console.log(`ERROR: Task ${program.task} is not supported!!`);
    process.exit(1);
}

if (program.mode && !validModes.includes(program.mode)) {
    console.log(`ERROR: Mode ${program.mode} is not supported!!`);
    process.exit(1);
}

if (program.hook && !fs.existsSync(path.join(process.cwd(), program.hook))) {
    console.log(`ERROR: file ${program.hook} not found in root folder!!`);
    process.exit(1);
}

if (program.promoteVersion && !validVersions.includes(program.promoteVersion)) {
    console.log(`ERROR: Version ${program.promoteVersion} is not supported!`);
    process.exit(1);
}
let task = program.task || validTasks[0];
let mode = program.mode || validModes[0];
let version = program.promoteVersion;
let serveqa = !!program.qa;
let analyze = !!program.analyze;
let watch = !!program.watch;
let open = !!program.open;
let hook = program.hook;
let appName = program.appName;
let appSrc = program.appSrc;
let branch = program.branch;
let zipPath = program.zipTarget;
let enterprisePath = program.enterprisePath;
let jshint = program.jshint;

webpack_setup({ task, mode, open, analyze, watch, serveqa, hook, version, appName, appSrc, branch, zipPath, enterprisePath, jshint });
