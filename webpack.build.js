'use strict';

const fs = require('fs');
const path = require('path');
const { LOADERS, getLoader } = require('./loaders');
const { PLUGINS, getPlugin } = require('./plugins');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer')
  .BundleAnalyzerPlugin;
const processPath = process.cwd();
const distPath = path.join(processPath, 'dist');

module.exports = function (env, argv) {
  let config = getConfig(argv.mode);
  if (fs.existsSync(path.resolve(processPath, 'help'))) {
    config.plugins.push(getPlugin(PLUGINS.CopyWebpackPlugin));
  }
  if (env) {
    if (env.analyze) {
      config.plugins.push(new BundleAnalyzerPlugin());
    }

    if (env.hook) {
      let hookjs = require(path.join(process.cwd(), env.hook));
      config = hookjs.processConfig(config);
    }
  }
  return config;
};

function getConfig(mode) {
  return {
    mode: mode || 'production',

    context: processPath,

    entry: ['core-js/fn/promise', './index.js'],

    output: {
      path: distPath,
      publicPath: '',
      filename: '[name].[hash].js',
      chunkFilename: '[name].[hash].js'
    },

    module: {
      rules: [
        getLoader(LOADERS.jsHint),
        getLoader(LOADERS.ts),
        getLoader(LOADERS.annotate),
        getLoader(LOADERS.babel, 'production'),
        getLoader(LOADERS.html),
        getLoader(LOADERS.css),
        getLoader(LOADERS.less),
        getLoader(LOADERS.file)
      ]
    },

    plugins: [
      getPlugin(PLUGINS.HtmlWebPackPlugin),
      getPlugin(PLUGINS.MiniCssExtractPlugin),
      getPlugin(PLUGINS.CleanWebpackPlugin),
      getPlugin(PLUGINS.NoEmitOnErrorsPlugin)
    ],

    optimization: {
      minimizer: [getPlugin(PLUGINS.UglifyJsPlugin)],
      runtimeChunk: {
        name: 'vendor'
      },
      splitChunks: {
        cacheGroups: {
          default: false,
          commons: {
            test: /node_modules\\(?!@clearsight).*/,
            name: 'vendor',
            chunks: 'initial',
            minSize: 1
          },
          cslibs: {
            test: /node_modules\\@clearsight.*/,
            name: 'cslibs',
            chunks: 'initial',
            minSize: 1
          }
        }
      }
    }
  };
}
