#! /usr/bin/env node
const path = require('path');
const fs = require('fs');
const getAppName = require('./common').getAppName;
const archiver = require('archiver');
const packageJson = path.join(process.cwd(), 'package.json');

module.exports = { zip }

function zip(options) {

    const outputDir = options.zipPath || 'dist';

    const p = path.join(process.cwd(), outputDir);
    const fileName = (options.appName || getAppName(packageJson)) + '.zip';
    const fullPath = path.join(p, fileName);

    process.chdir(p);
    if (fs.existsSync(fullPath)) {
        fs.unlinkSync(fullPath);
    }

    let archive = archiver.create('zip', { level: 1, archive: fileName });
    let dest = fileName;
    let output = fs.createWriteStream(dest);

    archive.on('error', (err) => {
        console.log('error: ');
        console.log(err);
        process.exit(-1);
    });

    archive.on('entry', (file) => {
        console.log('Archived ' + file.name);
    });

    output.on('close', () => {
        console.log('complete');
    });

    output.on('error', (err) => {
        console.log('error: ');
        console.log(err);
        process.exit(-1);
    });

    archive.pipe(output);

    function getSubFiles(dir) {
        let fullDir = path.join(p, dir);
        let files = fs.readdirSync(fullDir);
        for (let i = 0; i < files.length; i++) {
            let type = fs.statSync(path.join(fullDir, files[i]));
            let fileName = path.join(dir, files[i]);
            if (type.isFile()) {
                if (path.extname(fileName) !== '.zip') {
                    archive.file(dir + '/' + files[i], { name: files[i] });
                }
            } else if (type.isDirectory()) {
                archive.append(null, { name: files[i], type: 'directory' });
                getSubFiles(files[i]);
            }
        }

        //return srcArray;
    }


    fs.readdir(p, (err, items) => {
        if (err) {
            console.log(err);
            process.exit(-1);
        } else {
            for (let i = 0; i < items.length; i++) {
                let type = fs.statSync(path.join(p, items[i]));
                if (type) {
                    if (type.isFile()) {
                        if (path.extname(items[i]) !== '.zip') {
                            archive.file(items[i], { name: items[i] });
                        }
                    } else if (type.isDirectory()) {
                        archive.directory(items[i], { name: items[i] });
                    }

                }
            }
        }
        archive.finalize();
    });
}
